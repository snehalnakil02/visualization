## visualization

Visualization tools (structures, grid functions, Fermi surfaces,
phonon displacements, etc) by Andrei Postnikov

There are two subdirectories:

* Sies2xsf: Utilities to convert structure and grid files to the XSF format of XCrysden

* Sies2vesta: Utilities to convert structure and phonon eigenvector information to Vesta

(Work in progress to document and bring the programs up to date)
